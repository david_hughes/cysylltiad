# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import get_user_model


User = get_user_model()


class Message(models.Model):
    sender = models.ForeignKey(User, related_name='sender')
    recipient = models.ForeignKey(User, related_name='recipient')
    message_text = models.TextField(max_length=512)
    timestamp = models.DateTimeField(
        auto_now_add=True,
    )
    seen = models.BooleanField(default=True)

    def __str__(self):
        return "{} -> {}, {}".format(
            self.sender.email,
            self.recipient.email,
            self.timestamp
        )

    class Meta:
        ordering = ['timestamp']
