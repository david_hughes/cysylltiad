#!/bin/bash

#useradd nonrootuser  --home-dir /home/nonrootuser --groups sudo www-data \
#    --create-home --password <SECRET_PASSWORD>
# obviously don't want to be storing an actual password under VCS

apt update
apt install postgresql-10
apt install build-essential python
apt install python3-dev python3-pip nginx
apt install virtualenv
apt install uwsgi
apt install unzip

mkdir -p /srv/www/latest/

sudo -u postgres psql -c db_setup.sql

# TODO: Identify what further steps you'd need to perform in order to fully
# prefer server for deployment.

# TODO: Note that there appeared to be an issue with

