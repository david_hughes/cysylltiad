Chat App
========

## Quick Start Guide

(This has been tested on a machine running Ubuntu 18.04, with Postgres 10.4,
virtualenv and Python dev dependencies - e.g. python3-dev, build-essential,
installed. Haven't been able to test on other operating systems.)

Open a terminal; `cd` to the location where you cloned this project.
Run the following commands:

```$ chmod +x bootstrap.sh```

```./bootsrap.sh```

This *should* hopefully complete the following steps for you:
* Create a suitable named database, with a password matching that found in 
`project.settings.local`.
* Create a `virtualenv` located in the project directory.
* Activate the `virtualenv`, and install the project dependencies.
* Run migrations for the project.
* Import data from fixtures for the `User` and `UserProfile`
models.


Assuming the above command ran properly, you'll have to set the `DJANGO_SETTINGS_MODULE`
environment, like so:
```export DJANGO_SETTINGS_MODULE=project.settings.local```

Following the above steps, you should have a working development environment,
and the ability to log in using the following users:
* 'david.hughes'
* 'example.user.1'
* 'example.user.2'

each of which has the password `RQ2CrQVFcTJSdp8v` set.
(This is a randomly generated password 
that has only been used in this project.)

As ever, you may start a development server using:
```python manage.py runserver```

If you then navigate to `localhost:8000` you'll be prompted to log in.

At this point, you'll need to select an available user from the green box
to send a message to - clicking on their username will retrieve and display
any messages in your message history.

Then, if you wish to, you can send a message by typing it in, and pressing send.

The other user should receive this message, if they happen to be online at that
point within five seconds, assuming that `window.setInterval`'s time keeping is 
accurate and that the server responds to the request in a timely manner.
