# -*- coding: utf-8 -*-
"""
.. module:: api.apps
   :synopsis: AppConfig for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""


from __future__ import unicode_literals

from django.apps import AppConfig


class ApiConfig(AppConfig):
    """AppConfig for API app"""
    name = 'api'
