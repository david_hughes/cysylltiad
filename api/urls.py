# -*- coding: utf-8 -*-
"""
.. module:: api.urls
   :synopsis: URLs for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from django.conf.urls import url, include
from .routers import router

urlpatterns = [
    url(r'^', include(router.urls)),
]
