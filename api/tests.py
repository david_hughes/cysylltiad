# -*- coding: utf-8 -*-
"""
.. module:: api.tests
   :synopsis: Tests for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

# Note to self: My instinct tells me that this is the area of the app that
# should be subject to the most thorough testing.


from __future__ import unicode_literals

# from django.test import TestCase
# Create your tests here.
