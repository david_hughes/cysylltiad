# -*- coding: utf-8 -*-
"""
.. module:: api
   :synopsis: API app. As the name suggests, this app is home to all
              REST API-related functionality.
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""