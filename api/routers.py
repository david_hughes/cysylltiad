# -*- coding: utf-8 -*-
"""
.. module:: api.routers
   :synopsis: Django Rest Framework routers for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""


from rest_framework import routers

from api.viewsets import MessageViewSet

router = routers.DefaultRouter()
router.register(r'messages', MessageViewSet)
