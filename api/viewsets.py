# -*- coding: utf-8 -*-
"""
.. module:: api.viewsets
   :synopsis: Viewsets for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from django.contrib.auth import get_user_model
from django.db.models import Q

from dateutil import parser

from rest_framework import viewsets
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from api.serializers import MessageSerializer
from messaging.models import Message


User = get_user_model()


# pylint: disable=too-many-ancestors
class MessageViewSet(viewsets.ModelViewSet):
    """
    Viewset for Message model
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def list(self, request, *args, **kwargs):

        query_params = self.request.query_params
        timestamp_last_message = query_params.get('timestamp_last_message', '0')
        recipient_username = query_params.get('recipient_username', None)
        pagination_page = query_params.get('pagination_page', 'latest')
        queryset = self.filter_queryset(self.get_queryset())

        if recipient_username is not None:
            active_user = request.user

            try:
                recipient = User.objects.get(username=recipient_username)
            except User.DoesNotExist:
                raise ParseError("No user matching name {}".format(recipient_username))

            message_filter_1 = Q(sender=active_user, recipient=recipient)
            message_filter_2 = Q(sender=recipient, recipient=active_user)
            combined_message_filter = message_filter_1 | message_filter_2
            queryset = queryset.filter(combined_message_filter)

        if timestamp_last_message != '0':
            try:
                timestamp_last_message = parser.parse(timestamp_last_message)
            except TypeError:
                raise ParseError("Malformed timestamp: {} is not a recognised datetime format".format(
                    timestamp_last_message)
                )
            queryset = queryset.filter(timestamp__gt=timestamp_last_message)

        if pagination_page != 'latest':
            if type(pagination_page) != int:
                try:
                    pagination_page = int(pagination_page)
                except ValueError:
                    raise ParseError("Invalid value submitted for pagination_page: {}".format(
                        pagination_page
                    ))
            # TODO: integrate this with DRF's pagination to enable pagination

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
