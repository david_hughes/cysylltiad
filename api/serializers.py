# -*- coding: utf-8 -*-
"""
.. module:: api.serializers
   :synopsis: Django Rest Framework serializers for API app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from django.contrib.auth import get_user_model

from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from messaging.models import Message


User = get_user_model()

# pylint: disable=too-few-public-methods
class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for User model
    """
    class Meta:
        """
        Meta class for UserSerializer
        """
        model = User
        fields = (
            'id',
        )


class MessageSerializer(serializers.ModelSerializer):
    """
    Serializer for Message model
    """
    sender_username = serializers.CharField(source='sender.username')
    recipient_username = serializers.CharField(source='recipient.username')

    def create(self, validated_data):
        """
        To get this all to run smoothly - i.e. so that we can submit usernames
        rather than primary keys, we override the create method; this, I
        think, makes both the JavaScript and the Python code easier to debug.
        """

        request = self.context['request']
        sender = request.user

        recipient_data = validated_data.get('recipient', None)
        recipient_username = recipient_data.get('username', None)
        message_text = validated_data.get('message_text', None)

        if None in (
                recipient_username,
                message_text
        ):
            raise ValidationError("Malformed request: missing data")

        try:
            recipient = User.objects.get(username=recipient_username)
        except User.DoesNotExist:
            raise ValidationError("No such user: {}".format(recipient_username))

        message = Message.objects.create(
            sender=sender,
            recipient=recipient,
            message_text=message_text
        )
        return message

    # pylint:disable=too-few-public-methods
    class Meta:
        """
        Meta class for MessageSerializer
        """
        model = Message
        fields = (
            'sender_username',
            'recipient_username',
            'message_text',
            'timestamp'
        )
