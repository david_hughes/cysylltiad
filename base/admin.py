# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as UserAdmin_

from base.models import UserProfile

User = get_user_model()


class UserProfileInline(admin.TabularInline):
    """
    UserProfile inline.
    """
    model = UserProfile
    can_delete = False
    verbose_name_plural = 'User Profile'


class UserAdmin(UserAdmin_):
    inlines = (UserProfileInline, )

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super().get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
