# -*- coding: utf-8 -*-
"""
.. module:: code_quality
   :synopsis: Quick script to run pylint on *.py files across the project apps
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

import glob
import os
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils.translation import gettext as _

from base.exceptions import UserInputError

PROJECT_ROOT_PATH = settings.ROOT_DIR


class Command(BaseCommand):
    """
    Management command to run pylint across Python scripts on an app by app
    basis.

    Note that this is, at present, rather rudimentary - one weakness is that
    it outputs *A LOT* of text, which - means that it can be difficult
    to take everything in. Could do with work.
    """

    help = _('Usage: python manage.py code_quality --app_name=<app_name>')

    def add_arguments(self, parser):

        parser.add_argument(
            '--app_name',
            dest='app_name',
            default=None,
            help='Name of app to analyse'
        )

    def handle(self, *args, **options):

        app_name = options.get('app_name', None)
        # retrieve list of folders in project root

        if app_name is None:
            project_folders = set(next(os.walk(PROJECT_ROOT_PATH))[1])
        else:
            project_folders = set([app_name])

        installed_apps = set(settings.INSTALLED_APPS)

        project_apps = project_folders.intersection(installed_apps)

        if not project_apps:
            raise UserInputError("Specified project app could not be found.")

        for project_app in project_apps:
            py_filenames = glob.glob(
                '{}/**/*.py'.format(project_app),
                recursive=True
            )
            for py_filename in py_filenames:
                print("Checking {} with pylint:-".format(py_filename))
                try:
                    subprocess.check_output(['pylint', py_filename])
                except subprocess.CalledProcessError as error:
                    for line in error.output.split(b'\n'):
                        output = line.decode("utf-8")
                        print("{}".format(output))
                else:
                    print("No issues! :)")
                print("\n")
