# -*- coding: utf-8 -*-
"""
.. module:: base.urls
   :synopsis: Base URLs
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from django.conf.urls import url
from .views import HomePageView, KnockoutSandboxView

urlpatterns = [
    url(r'^$', HomePageView.as_view()),
    url(r'^knockout_sandbox/$', KnockoutSandboxView.as_view()),
]
