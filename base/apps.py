# -*- coding: utf-8 -*-
"""
.. module:: base.apps
   :synopsis: AppConfig for base app
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""


from __future__ import unicode_literals

from django.apps import AppConfig


class BaseConfig(AppConfig):
    """AppConfig for base app"""
    name = 'base'
