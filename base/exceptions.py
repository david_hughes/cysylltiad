# -*- coding: utf-8 -*-
"""
.. module:: base.exceptions
   :synopsis: Core custom exceptions, for use across the project
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""


class UserInputError(ValueError):
    """
    Custom Exception to use in the event of a user inputting invalid input
    """
    pass
