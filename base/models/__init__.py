# -*- coding: utf-8 -*-
"""
.. module:: base.models
   :synopsis: Base models:
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from .userprofile import UserProfile

__all__ = [
    UserProfile
]
