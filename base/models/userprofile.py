# -*- coding: utf-8 -*-
"""
.. module:: base.models.userprofile
   :synopsis: Base models
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""

from django.conf import settings
from django.db import models


class UserProfile(models.Model):
    """
    Model to add additional fields to User object,
    mostly related to chat-specific functionality.
    """

    is_available = models.BooleanField(default=False)
    last_seen = models.DateTimeField(blank=True, null=True)
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        blank=False
    )

    def __str__(self):
        """
        String representation for UserProfile
        """
        return self.user.username
