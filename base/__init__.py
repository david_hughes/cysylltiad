# -*- coding: utf-8 -*-
"""
.. module:: base
   :synopsis: base app.
              This app is intended to be a home for what I would term core
              items - that is to say, things like:-
              - helper functions
              - base templates
              - generic views
              - static assets
              As a rule of thumb, if you can reasonably be expect something
              to be used in multiple apps, this is the logical place to put
              it.
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""
