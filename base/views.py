# -*- coding: utf-8 -*-
"""
.. module:: base.views
   :synopsis: Core tests
.. moduleauthor:: David Hughes <dghughes82@gmail.com>
"""


from __future__ import unicode_literals

from braces.views import LoginRequiredMixin
from django.views.generic import TemplateView

from base.models import UserProfile


class HomePageView(LoginRequiredMixin, TemplateView):
    """
    Template view for project home page.
    """
    template_name = 'base/home.html'
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_user = self.request.user
        available_users = UserProfile.objects.filter(
            is_available=True
        ).exclude(user=current_user)
        context['available_users'] = available_users
        return context


class KnockoutSandboxView(TemplateView):
    """
    Template view for project home page.
    """
    template_name = 'base/knockout_sandbox.html'
