#!/usr/bin/env bash

# bootstrap script (tested on Ubuntu 18.04 - assumes that postgres,
# virtualenv are already installed.

# - Create database
psql < db_setup.sql
# - Create virtualenv
virtualenv --python=/usr/bin/python3 .venv
source .venv/bin/activate

# - Install requirements
pip install -r requirements-dev.txt
export DJANGO_SETTINGS_MODULE=project.settings.local

# - Run migrations
python manage.py migrate
# - Install user and user profile model fixtures
# This creates users with usernames 'david.hughes', 'example.user.1',
# and 'example.user.2' - all use the password 'RQ2CrQVFcTJSdp8v'.
python manage.py loaddata user.json
python manage.py loaddata userprofile.json

